import { defineConfig } from 'astro/config';
import { VitePWA } from "vite-plugin-pwa";

// https://astro.build/config
export default defineConfig({
  vite: {
    plugins: [
      VitePWA({
        registerType: "autoUpdate",
        strategies: "generateSW", // default
        injectRegister: "script-defer", // to inject registerSW property in pwaInfo object
        devOptions: { enabled: true }, // no need because build but...

      })
    ]
  }
});
