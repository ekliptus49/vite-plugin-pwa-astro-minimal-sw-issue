import { registerSW } from "virtual:pwa-register";

const _updateSW = registerSW({
  immediate: true,
  onOfflineReady() {
    console.log("updateSW - offline ready");
  },
  onNeedRefresh() {
    console.log("updateSW - need refresh !");
  },
  onRegisterError(error) {
    console.log("updateSW - register error : \n", error);
  },
  onRegisteredSW(swScriptUrl, registration) {
    console.log(
      "updateSW - registered sw : \n",
      swScriptUrl, "\n",
      registration,
    );
  },
  // onRegistered(registration) {
  //   console.log("updateSW - registered : \n", registration);
  // },
});
