# Minimal astro + vitePWA to reproduce issue

## TLDR

Issue link : https://github.com/vite-pwa/vite-plugin-pwa/issues/694

Subject :
`pnpm build` do not generate SW.js (and workbox-x.js) when `registerSW()` function is in `pwa.js` that was conditionally (true) import in html (see index.astro file).

I guess vite-plugin-pwa try to determine if the registerSW() was triggered before generate SW.
And the conditionnal html script import in `{...}` isn't follow.. ?

I don't have found any " registerSW() triggered flag " on vite-plugin-pwa source code.

Why registerSW() call is needed to generate SW.js with genrateSW strategie ???

## Step to reproduce :

1. `pnpm create astro@latest --template minimal`
2. add dependencies :
    - `pnpm add -D vite-plugin-pwa`
    - `pnpm add -D workbox-window` (needed to generateSW ? I dont know why it is not include with vite-plugin-pwa ? )
3. `astro.config.mjs` VitePWA config add (see bottom)
4. modify `index.astro`
   - add manifest link injection
   - add registerSW.js import injection
   - add `pwa.js` import (with registerSW() function call)
5. add `pwa.js` file
6. optional tsconfig.json ajustment
7. optional package.json build script
8. `pnpm build`
9. **Check `dist/`**


```bash
/
├── dist/ <<< # here the result
├── public/
├── src/
│   └── pages/
│   │   └── index.astro <<< # script import problem
|   └── pwa.js # script that have registerSW()
└── package.json
```

